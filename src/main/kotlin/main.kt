import java.util.ArrayDeque
import kotlin.math.max
import kotlin.text.StringBuilder

fun main() {
    var text = ""
    while (text.isEmpty()) {
        print("enter text: ")
        text = readln()
    }
    println("text: $text")

    val charsWithOccurrences = text
        .groupBy { it }
        .mapValues { it.value.count() }
        .toList()
        .sortedBy { it.second }
    println("charsWithOccurrences: $charsWithOccurrences")

    val tree = createHuffmanTree(charsWithOccurrences)
    println("tree: $tree")

    val codingScheme = getCodingScheme(tree)
    println("codingScheme: $codingScheme")

    val encodedText = text.map { codingScheme[it] }.joinToString("")
    println("encodedText: $encodedText")

    val decodedTex = decode(encodedText, codingScheme)
    println("decodedText: $decodedTex")

    println()
    charsWithOccurrences
        .associate { it.first to it.second }
        .mapValues { (char, occurrence) -> listOf(occurrence.toString(), codingScheme[char].toString()) }
        .mapKeys { (key, _) -> "'$key'" }
        .let { prettyPrintTable(it, listOf("char", "count", "code")) }
    println()
}

fun createHuffmanTree(charsWithOccurrences: List<Pair<Char, Int>>): HuffmanTree {
    val tree: HuffmanTree = mutableListOf<HuffmanTree>(HuffmanTree.Leaf(charsWithOccurrences.first()))
        .also { trees ->
            val remainingPairsOfCharAndOccurrence = charsWithOccurrences.subList(1, charsWithOccurrences.size).toMutableList()
            while (remainingPairsOfCharAndOccurrence.isNotEmpty()) {
                trees.sortBy { it.count }
                val costOfCombiningFirstTreeWithNextPair = trees.first().count + remainingPairsOfCharAndOccurrence.first().second
                val costOfCombiningTwoPairs =
                    if (remainingPairsOfCharAndOccurrence.size >= 2)
                        remainingPairsOfCharAndOccurrence.first().second + remainingPairsOfCharAndOccurrence[1].second
                    else Int.MAX_VALUE
                val costOfCombiningTwoTrees = if (trees.size > 1) trees.first().count + trees[1].count else Int.MAX_VALUE

                when (listOf(costOfCombiningFirstTreeWithNextPair, costOfCombiningTwoPairs, costOfCombiningTwoTrees).min()) {
                    costOfCombiningFirstTreeWithNextPair -> trees[0] = HuffmanTree.Node(
                        trees[0],
                        HuffmanTree.Leaf(remainingPairsOfCharAndOccurrence.removeFirst())
                    )
                    costOfCombiningTwoPairs -> trees.add(HuffmanTree.Node(HuffmanTree.Leaf(
                        remainingPairsOfCharAndOccurrence.removeFirst()),
                        HuffmanTree.Leaf(remainingPairsOfCharAndOccurrence.removeFirst()))
                    )
                    costOfCombiningTwoTrees -> trees[0] = HuffmanTree.Node(trees[0], trees[1]).also { trees.removeAt(1) }
                }
            }
        }
        .reduce { a, b -> HuffmanTree.Node(a, b) }

    return tree
}

fun getCodingScheme(tree: HuffmanTree): Map<Char, String> {
    if (tree is HuffmanTree.Leaf)
        return mapOf(tree.value.first to "0")

    val stack = ArrayDeque<Pair<HuffmanTree, String>>()
        .apply { (tree as HuffmanTree.Node).next.toList().forEachIndexed { i, it -> add(it to i.toString()) } }
    val out: MutableMap<Char, String> = mutableMapOf()

    while (stack.isNotEmpty()) {
        val (nextTree, nextPrefix) = stack.pollFirst()!!
        when (nextTree) {
            is HuffmanTree.Node -> nextTree.next.toList()
                .forEachIndexed { i, it -> stack.push(it to nextPrefix + i.toString()) }

            is HuffmanTree.Leaf -> out[nextTree.value.first] = nextPrefix
        }
    }

    return out
}

fun decode(encodedText: String, codingScheme: Map<Char, String>): String {
    val map = codingScheme.toList().sortedBy { it.second.length }
    var remainder = encodedText
    val out = StringBuilder()

    while (remainder.isNotEmpty()) {
        map.forEach { (char, code) ->
            if (remainder.startsWith(code)) {
                remainder = remainder.substring(code.length)
                out.append(char)
            }
        }
    }

    return out.toString()
}

//fun prettyPrint(tree: HuffmanTree) {
//    // TODO: implement
//}

fun <T, S>prettyPrintTable(table: Map<T, List<S>>, header: List<String>) {
    val keys = table.keys
    val values = table.values
    val maxEntrySize: Int  = max(
            values.maxOf { it.maxOf { entry -> entry.toString().length } },
            keys.maxOf { it.toString().length }
        )
    fun <V>addSpacing(it: V, space: Int): String = " ".repeat(space - it.toString().length) + it.toString()
    val spaceForHeaders = header.maxOf { it.length }
    val out = StringBuilder()

    out.append("| ")
    out.append(addSpacing(header.first(), spaceForHeaders))
    out.append(" | ")
    out.append(keys.joinToString(separator = " | ") { addSpacing(it, maxEntrySize) })
    out.append(" |")
    out.append(System.lineSeparator())

    for (i in (0..values.first().lastIndex)) {
        out.append("| ")
        out.append(addSpacing(header[i+1], spaceForHeaders))
        out.append(" | ")
        values.map { it[i] }
            .let { row ->
                out.append(row.joinToString(separator = " | ") { addSpacing(it, maxEntrySize) })
            }
        out.append(" |")

        if (i + 1 <= values.first().lastIndex) out.append(System.lineSeparator())
    }

    println(out.toString())
}

sealed interface HuffmanTree {
    class Node(
        rightSide: HuffmanTree,
        leftSide: HuffmanTree,
    ) : HuffmanTree {
        val next: Pair<HuffmanTree, HuffmanTree> = rightSide to leftSide

        override val count: Int = next.first.count + next.second.count
        override fun toString(): String = "[Node: $count, ${next.first}, ${next.second}]"
    }

    class Leaf(
        val value: Pair<Char, Int>
    ) : HuffmanTree {
        override val count: Int
            get() = value.second

        override fun toString(): String = "[Leaf: ${value}]"
    }

    val count: Int
    override fun toString(): String
}

fun <T> Pair<T, T>.toList() = listOf(this.first, this.second)